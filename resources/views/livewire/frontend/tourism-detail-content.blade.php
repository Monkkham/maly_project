<div>
    <!-- Header Start -->
    <div class="container-fluid bg-breadcrumb">
        <div class="container text-center py-5" style="max-width: 900px;">
            <h4 class="text-white display-3 mb-4"><i class="fas fa-eye"></i> ລາຍລະອຽດ / {{ $this->name }}</h4>
            <ol class="breadcrumb justify-content-center mb-0">
                <li class="breadcrumb-item"><a href="{{ route('frontend.home') }}">ໜ້າຫຼັກ</a></li>
                <li class="breadcrumb-item active text-white">ລາຍລະອຽດ</li>
            </ol>
        </div>
    </div>
    <!-- Header End -->
    <!-- About Start -->
    <div class="container-fluid about py-5">
        <div class="container py-5">
            <div class="row g-5 align-items-center">
                <div class="col-lg-5">
                    <div class="h-100"
                        style="border: 50px solid; border-color: transparent #13357B transparent #13357B;">
                        <img src="{{ asset($this->image) }}" class="img-fluid w-100 h-100" alt="">
                    </div>
                </div>
                <div class="col-lg-7"
                    style="background: linear-gradient(rgba(255, 255, 255, .8), rgba(255, 255, 255, .8)), url(img/about-img-1.png);">
                    <h1 class="mb-4">{{ $this->name }}</span></h1>
                    <div class="row gy-2 gx-4 mb-4">
                        <div class="col-sm-12">
                            <p class="mb-0"><i class="fa fa-map-marked-alt text-primary me-2"></i>
                                @if (!empty($province))
                                    ທີ່ບ້ານ: {{ $village }} -
                                    ເມືອງ: {{ $district }} -
                                    ແຂວງ: {{ $province }}
                                @endif
                            </p>
                            <small class="text-uppercase">
                               <i class="fas fa-clock"></i> ເຜີຍແຜ່ເເລ້ວ: {{ $this->created_at->diffForHumans() }}</small> <br>
                               <small class="text-uppercase">
                                <i class="fas fa-eye"></i> ຍອດວິວເຂົ້າຊົມ: {{ $this->viewer }} ຄັ້ງ</small>
                        </div>
                    </div>
                    <p class="mb-4">
                        {!! $this->note !!}
                    </p>
                </div>
            </div>
        </div>
    </div>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2KPqF6KV_ch-jdx6hSVNQXhezl2mcuzk&libraries=places&callback=initMap"
        type="text/javascript"></script>
    <tbody>
        <div class="text-center">
            @if(!empty($this->latitude) && !empty($this->longitude))
            <iframe  src="https://maps.google.com/maps?q={{$this->latitude}},{{$this->longitude}}&hl=es;z=13&output=embed" style="height:400px; width: 100%;"></iframe>
            @else
            <td class="text-center bg-danger"><i class="fa fa-map-marker-alt"></i> {{__('lang.no store location')}}</td>
            @endif
        </div>
    </tbody>
    <!-- Gallery Start -->
    <div class="container-fluid gallery py-5 my-5">
        <div class="mx-auto text-center mb-5" style="max-width: 900px;">
            <h5 class="section-title px-3">ຮູບພາບປະກອບ</h5>
            <h1 class="mb-4">{{ $this->tourism_type }}</h1>
            </p>
        </div>
        <div class="tab-class text-center">
                <div class="tab-content">
                    <div id="GalleryTab-1" class="tab-pane fade show p-0 active">
                        <div class="row g-2">
                            @foreach ($images->tourism_images as $item)
                            <div class="col-sm-6 col-md-6 col-lg-4 col-xl-3">
                                <div class="gallery-item h-100">
                                    <img src="{{ asset($item->photo) }}" class="img-fluid w-100 h-100 rounded" alt="Image">
                                    <div class="gallery-content">
                                        <div class="gallery-info">
                                            <h5 class="text-white text-uppercase mb-2">Tour In Laos</h5>
                                            <a href="{{ asset($item->photo) }}" class="btn-hover text-white">ເບິ່ງຮູບພາບ <i
                                                    class="fa fa-arrow-right ms-2"></i></a>
                                        </div>
                                    </div>
                                    <div class="gallery-plus-icon">
                                        <a href="{{ asset($item->photo) }}" data-lightbox="gallery-2" class="my-auto"><i
                                                class="fas fa-plus fa-2x text-white"></i></a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <!-- Gallery End -->
</div>
