<div>
    <!-- Header Start -->
    <div class="container-fluid bg-breadcrumb">
        <div class="container text-center py-5" style="max-width: 900px;">
            <h3 class="text-white display-3 mb-2"><i class="fas fa-user-plus"></i> ເຂົ້າສູ່ລະບົບ</h1>
                <ol class="breadcrumb justify-content-center mb-0">
                    <li class="breadcrumb-item"><a href="{{ route('frontend.home') }}">ໜ້າຫຼັກ</a></li>
                    <li class="breadcrumb-item active text-white">ເຂົ້າສູ່ລະບົບ</li>
                </ol>
        </div>
    </div>
    <!-- Header End -->
    <!-- Contact Start -->
    <div class="container-fluid contact bg-light py-5">
        <div class="container py-5">
            <div class="mx-auto text-center mb-1" style="max-width: 900px;">
                <h5 class="section-title px-3"><i class="fas fa-sign-in-alt"></i> ເຂົ້າສູ່ລະບົບ</h5>
            </div>
            <div class="row g-5 align-items-center">
                <div class="col-lg-12">
                    <form>
                        <div class="row g-3">
                            <div class="col-md-12">
                                <div class="form-floating">
                                    <input wire:model='phone' type="number" min="1"
                                        class="form-control border-0 @error('phone') is-invalid @enderror"
                                        id="phone" placeholder="Your phone">
                                    @error('phone')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                    <label for="phone">ເບີໂທ</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-floating">
                                    <input wire:model='password' type="password"
                                        class="form-control border-0 @error('subject') is-invalid @enderror"
                                        id="password" placeholder="Your password">
                                    @error('password')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                    <label for="password">ລະຫັດຜ່ານ</label>
                                </div>
                            </div>
                            <div class="form-group d-md-flex">
                                <div class="w-50">
                                    <label class="checkbox-wrap checkbox-primary">ຈື່ຈຳຂ້ອຍໄວ້</label>
                                        <input style="transform: scale(1.5);" type="checkbox" id="remember" wire:model.live="remember">
                                        <span class="checkmark"></span>
                                </div>
                            </div>
                            <div class="col-12 text-center">
                                <button wire:click.live="Signin" class="btn btn-primary w-100 py-3"
                                    type="button"><i class="fas fa-sign-in-alt text-white"></i>
                                    ເຂົ້າສູ່ລະບົບ</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Contact End -->
</div>
