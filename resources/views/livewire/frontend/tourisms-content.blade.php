<div>
    <!-- Header Start -->
    <div class="container-fluid bg-breadcrumb">
        <div class="container text-center py-5" style="max-width: 900px;">
            <h3 class="text-white display-3 mb-4"><i class="fas fa-globe"></i> ແຫຼ່ງທ່ອງທ່ຽວ</h1>
                <ol class="breadcrumb justify-content-center mb-0">
                    <li class="breadcrumb-item"><a href="{{ route('frontend.home') }}">ໜ້າຫຼັກ</a></li>
                    <li class="breadcrumb-item active text-white">ແຫຼ່ງທ່ອງທ່ຽວ</li>
                </ol>
        </div>
    </div>
    <!-- Header End -->
    <!-- Blog Start -->
    <div class="container-fluid blog py-5">
        <div class="container py-5">
            <div class="mx-auto text-center mb-5" style="max-width: 900px;">
                <h5 class="section-title px-3">ແຫຼ່ງທ່ອງທ່ຽວ</h5>
                <h1 class="mb-4">ແຫຼ່ງທ່ອງທ່ຽວຂອງແຂວງຫົວພັນ</h1>
            </div>
            <div class="row g-4 justify-content-center">
                @foreach ($all_tourism as $item)
                    <div class="col-lg-4 col-md-6">
                        <div class="blog-item">
                            <div class="blog-img">
                                <div class="blog-img-inner">
                                    <img class="img-fluid w-100 rounded-top" src="{{ asset($item->image) }}"
                                        alt="Image" style="width: 100%; height:450px">
                                    <div class="blog-icon">
                                        <a href="#" class="my-auto"><i
                                                class="fas fa-link fa-2x text-white"></i></a>
                                    </div>
                                </div>
                                <div class="blog-info d-flex align-items-center border border-start-0 border-end-0">
                                    <small class="flex-fill text-center border-end py-2"><i
                                            class="fas fa-map-marked text-white me-2"></i>
                                        @if (!empty($item->village))
                                            {{ $item->village->name_la }}-
                                        @endif
                                        @if (!empty($item->district))
                                            {{ $item->district->name_la }}-
                                        @endif
                                        @if (!empty($item->province))
                                            {{ $item->province->name_la }}
                                        @endif
                                    </small>
                                </div>
                            </div>
                            <div class="blog-content border border-top-0 rounded-bottom p-4">
                                <small class="text-primary"><i class="fas fa-eye"></i> ຍອດວິວເຂົ້າຊົມ 
                                    @if($item->viewer)
                                        {{ $item->viewer }}
                                        @else
                                        0
                                    @endif
                                    ຄັ້ງ</small> <br>
                                <p class="mb-3">
                                    ເຜີຍແຜ່ເເລ້ວ:
                                    {{ $item->created_at->diffForHumans() }} </p>
                                <a href="#" class="h4">{{ $item->name }}</a>
                                {{-- <p class="my-3">
                                    {!! $item->note !!}
                                </p> --}}
                                <a href="#" wire:click='DetailTourism({{ $item->id }})' class="btn btn-primary rounded-pill py-2 px-4"><i
                                        class="fas fa-eye"></i> ອ່ານເພີ່ມເຕີມ</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- Blog End -->
</div>
