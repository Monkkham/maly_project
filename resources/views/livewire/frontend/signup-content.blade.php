<div>
    <!-- Header Start -->
    <div class="container-fluid bg-breadcrumb">
        <div class="container text-center py-5" style="max-width: 900px;">
            <h3 class="text-white display-3 mb-2"><i class="fas fa-user-plus"></i> ລົງທະບຽນ</h1>
                <ol class="breadcrumb justify-content-center mb-0">
                    <li class="breadcrumb-item"><a href="{{ route('frontend.home') }}">ໜ້າຫຼັກ</a></li>
                    <li class="breadcrumb-item active text-white">ລົງທະບຽນ</li>
                </ol>
        </div>
    </div>
    <!-- Header End -->
    <!-- Contact Start -->
    <div class="container-fluid contact bg-light py-5">
        <div class="container py-5">
            <div class="mx-auto text-center mb-1" style="max-width: 900px;">
                <h5 class="section-title px-3"><i class="fas fa-user-plus"></i> ລົງທະບຽນໃຫມ່</h5>
            </div>
            <div class="row g-5 align-items-center">
                <div class="col-lg-12">
                    <form>
                        <div class="row g-3">
                            <div class="col-md-12">
                                <div class="form-floating">
                                    <input wire:model='name_lastname' type="text"
                                        class="form-control border-0 @error('name_lastname') is-invalid @enderror"
                                        id="name_lastname" placeholder="Your Name_lastname">
                                    @error('name_lastname')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                    <label for="name_lastname">ຊື່ ນາມສະກຸນ</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-floating">
                                    <input wire:model='phone' type="number"
                                        class="form-control border-0 @error('phone') is-invalid @enderror"
                                        id="phone" placeholder="Your phone">
                                    @error('phone')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                    <label for="phone">ເບີໂທ</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input wire:model='password' type="password"
                                        class="form-control border-0 @error('subject') is-invalid @enderror"
                                        id="password" placeholder="Your password">
                                    @error('password')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                    <label for="password">ລະຫັດຜ່ານ</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input wire:model='confirmPassword' type="password"
                                        class="form-control border-0 @error('subject') is-invalid @enderror"
                                        id="confirmPassword" placeholder="Your confirmPassword">
                                    @error('confirmPassword')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                    <label for="confirmPassword">ຍືນຍັນລະຫັດຜ່ານ</label>
                                </div>
                            </div>
                            <div class="col-12">
                                <button wire:click.live="SignUp" class="btn btn-primary w-100 py-3"
                                    type="button"><i class="fas fa-user-plus text-white"></i>
                                    ລົງທະບຽນ</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Contact End -->
</div>
