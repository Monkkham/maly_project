<div>
    <!-- Header Start -->
    <div class="container-fluid bg-breadcrumb">
        <div class="container text-center py-5" style="max-width: 900px;">
            <h3 class="text-white display-3 mb-4"><i class="fas fa-globe"></i> ແຫຼ່ງທ່ອງທ່ຽວ</h1>
                <ol class="breadcrumb justify-content-center mb-0">
                    <li class="breadcrumb-item"><a href="{{ route('frontend.home') }}">ໜ້າຫຼັກ</a></li>
                    <li class="breadcrumb-item active text-white">ແຫຼ່ງທ່ອງທ່ຽວ</li>
                </ol>
        </div>
    </div>
    <!-- Header End -->
    <!-- Blog Start -->
    <div class="container-fluid blog py-5">
        <div class="container py-5">
            <div class="mx-auto text-center mb-5" style="max-width: 900px;">
                <h5 class="section-title px-3">ແຫຼ່ງທ່ອງທ່ຽວ</h5>
                <h1 class="mb-4">ແຫຼ່ງທ່ອງທ່ຽວຂອງແຂວງຫົວພັນ</h1>
            </div>
            <div class="row g-4 justify-content-center">
                @if ($data->count() > 0)
                    @foreach ($data as $item)
                        <div class="col-lg-4 col-md-6">
                            <div class="blog-item">
                                <div class="blog-img">
                                    <div class="blog-img-inner">
                                        <img class="img-fluid w-100 rounded-top" src="{{ asset($item->image) }}"
                                            alt="Image">
                                        <div class="blog-icon">
                                            <a href="#" class="my-auto"><i
                                                    class="fas fa-link fa-2x text-white"></i></a>
                                        </div>
                                    </div>
                                    <div class="blog-info d-flex align-items-center border border-start-0 border-end-0">
                                        <small class="flex-fill text-center border-end py-2"><i
                                                class="fas fa-map-marked text-white me-2"></i>
                                            @if (!empty($item->village))
                                                {{ $item->village->name_la }}-
                                            @endif
                                            @if (!empty($item->district))
                                                {{ $item->district->name_la }}-
                                            @endif
                                            @if (!empty($item->province))
                                                {{ $item->province->name_la }}
                                            @endif
                                        </small>
                                    </div>
                                </div>
                                <div class="blog-content border border-top-0 rounded-bottom p-4">
                                    <p class="mb-3">
                                        ເຜີຍແຜ່ເເລ້ວ:
                                        {{ $item->created_at->diffForHumans() }} </p>
                                    <a href="#" class="h4">{{ $item->name }}</a>
                                    <p class="my-3">
                                        {!! $item->note !!}
                                    </p>
                                    <a href="#" wire:click='DetailTourism({{ $item->id }})'
                                        class="btn btn-primary rounded-pill py-2 px-4"><i class="fas fa-eye"></i>
                                        ອ່ານເພີ່ມເຕີມ</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <style>
                        @import url(http://fonts.googleapis.com/css?family=Calibri:400,300,700);

                        body {
                            background-color: #eee;
                            font-family: 'Calibri', sans-serif !important;
                        }

                        .mt-100 {
                            margin-top: 10px;

                        }


                        .card {
                            margin-bottom: 30px;
                            border: 0;
                            -webkit-transition: all .3s ease;
                            transition: all .3s ease;
                            letter-spacing: .5px;
                            border-radius: 8px;
                            -webkit-box-shadow: 1px 5px 24px 0 rgba(68, 102, 242, .05);
                            box-shadow: 1px 5px 24px 0 rgba(68, 102, 242, .05);
                        }

                        .card .card-header {
                            background-color: #fff;
                            border-bottom: none;
                            padding: 24px;
                            border-bottom: 1px solid #f6f7fb;
                            border-top-left-radius: 8px;
                            border-top-right-radius: 8px;
                        }

                        .card-header:first-child {
                            border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0;
                        }

                        .card .card-body {
                            padding: 30px;
                            background-color: transparent;
                        }
                    </style>
                    <div class="container-fluid text-center">
                        <div class="row">

                            <div class="col-md-12">

                                <div class="card">
                                    <div class="card-body cart">
                                        <div class="col-sm-12 empty-cart-cls text-center">
                                            <img src="https://cdn.dribbble.com/users/1785628/screenshots/5605512/media/097297f8e21d501ba45d7ce437ed77bd.gif"
                                                style="width: auto; height:200px; margin-left: 38%">
                                            <h3><strong><i class="fas fa-search"></i>
                                                    ບໍ່ພົບຂໍ້ມູນທີ່ທ່ານຄົ້ນຫາ!</strong></h3>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                @endif
            </div>
        </div>
    </div>
    <!-- Blog End -->
</div>
