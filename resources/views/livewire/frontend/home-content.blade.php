<div>
    <!-- Carousel Start -->
    <div class="carousel-header">
        <div id="carouselId" class="carousel slide" data-bs-ride="carousel">
            <ol class="carousel-indicators">
                @foreach ($data as $index => $item)
                    <li data-bs-target="#carouselId" data-bs-slide-to="{{ $index }}"
                        @if ($index === 0) class="active" @endif></li>
                @endforeach
            </ol>
            <div class="carousel-inner">
                @foreach ($data as $index => $item)
                    <div class="carousel-item @if ($index === 0) active @endif">
                        <img src="{{ asset($item->image) }}" class="img-fluid" alt="Image">
                        <div class="carousel-caption">
                            <div class="p-3" style="max-width: 900px;">
                                <h1 class="display-2 text-capitalize text-white mb-4">{{ $item->header }}</h1>
                                <p class="mb-5 fs-5">{{ $item->body }}</p>
                                <div class="d-flex align-items-center justify-content-center">
                                    <a class="btn-hover-bg btn btn-primary rounded-pill text-white py-3 px-5"
                                        href="{{ route('frontend.tourism') }}">ແຫຼ່ງທ່ອງທ່ຽວ <i class="fas fa-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselId" data-bs-slide="prev">
                <span class="carousel-control-prev-icon btn bg-primary" aria-hidden="false"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselId" data-bs-slide="next">
                <span class="carousel-control-next-icon btn bg-primary" aria-hidden="false"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </div>

@include('livewire.frontend.action-search-content')

    <!-- ທຳມະຊາດ -->
    <div class="container-fluid packages pb-3">
        <div class="container py-3">
            <div class="mx-auto text-center mb-5" style="max-width: 900px;">
                <h5 class="section-title px-3">ແຫຼ່ງທ່ອງທ່ຽວ</h5>
                <h1 class="mb-0">{{ $this->tourism_type_natures->name_la }}</h1>
            </div>
            <div class="packages-carousel owl-carousel">
                @foreach ($tourism_type_nature as $item)
                    <div class="packages-item">
                        <div class="packages-img">
                            <img src="{{ asset($item->image) }}" class="img-fluid w-100 rounded-top" alt="Image" style="width: 100%; height:450px">
                            <div class="packages-info d-flex border border-start-0 border-end-0 position-absolute"
                                style="width: 100%; bottom: 0; left: 0; z-index: 5;">
                                <small class="flex-fill text-center border-end py-2"><i
                                        class="fa fa-map-marker-alt me-2"></i>
                                    @if (!empty($item->village))
                                        {{ $item->village->name_la }}-
                                    @endif
                                    @if (!empty($item->district))
                                        {{ $item->district->name_la }}-
                                    @endif
                                    @if (!empty($item->province))
                                        {{ $item->province->name_la }}
                                    @endif
                                </small>
                            </div>
                            <div class="packages-price py-2 px-4">
                                @if (!empty($item->tourism_type))
                                    {{ $item->tourism_type->name_la }}
                                @endif
                            </div>
                        </div>
                        <div class="packages-content bg-light">
                            <div class="p-4 pb-0">
                                <h5 class="mb-0">{{ $item->name }}</h5>
                                <small class="text-uppercase">ເຜີຍແຜ່ເເລ້ວ:
                                    {{ $item->created_at->diffForHumans() }}</small>
                                <div class="mb-3">
                                    <small class="text-primary"><i class="fas fa-eye"></i> ຍອດວິວເຂົ້າຊົມ 
                                        @if($item->viewer)
                                            {{ $item->viewer }}
                                            @else
                                            0
                                        @endif
                                        ຄັ້ງ</small>
                                </div>
                                {{-- <p class="mb-4">
                                    {!! $item->note !!}
                                </p> --}}
                            </div>
                            <div class="row bg-primary rounded-bottom mx-0">
                                <div class="col-12 text-center px-0">
                                    <a wire:click='DetailTourism({{ $item->id }})'
                                        class="btn-hover btn text-white py-2 px-4"><i class="fas fa-eye"></i>
                                        ລາຍລະອຽດ</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- Packages End -->
    <!-- ປະຫວັດສາດ -->
    <div class="container-fluid packages pb-3">
        <div class="container py-3">
            <div class="mx-auto text-center mb-5" style="max-width: 900px;">
                <h5 class="section-title px-3">ແຫຼ່ງທ່ອງທ່ຽວ</h5>
                <h1 class="mb-0">{{ $this->tourism_type_historys->name_la }}</h1>
            </div>
            <div class="packages-carousel owl-carousel">
                @foreach ($tourism_type_history as $item)
                    <div class="packages-item">
                        <div class="packages-img">
                            <img src="{{ asset($item->image) }}" class="img-fluid w-100 rounded-top" alt="Image" style="width: 100%; height:450px">
                            <div class="packages-info d-flex border border-start-0 border-end-0 position-absolute"
                                style="width: 100%; bottom: 0; left: 0; z-index: 5;">
                                <small class="flex-fill text-center border-end py-2"><i
                                        class="fa fa-map-marker-alt me-2"></i>
                                    @if (!empty($item->village))
                                        {{ $item->village->name_la }}-
                                    @endif
                                    @if (!empty($item->district))
                                        {{ $item->district->name_la }}-
                                    @endif
                                    @if (!empty($item->province))
                                        {{ $item->province->name_la }}
                                    @endif
                                </small>
                            </div>
                            <div class="packages-price py-2 px-4">
                                @if (!empty($item->tourism_type))
                                    {{ $item->tourism_type->name_la }}
                                @endif
                            </div>
                        </div>
                        <div class="packages-content bg-light">
                            <div class="p-4 pb-0">
                                <h5 class="mb-0">{{ $item->name }}</h5>
                                <small class="text-uppercase">ເຜີຍແຜ່ເເລ້ວ:
                                    {{ $item->created_at->diffForHumans() }}</small>
                                <div class="mb-3">
                                    <small class="text-primary"><i class="fas fa-eye"></i> ຍອດວິວເຂົ້າຊົມ 
                                        @if($item->viewer)
                                            {{ $item->viewer }}
                                            @else
                                            0
                                        @endif
                                        ຄັ້ງ</small>
                                </div>
                                {{-- <p class="mb-4">
                                    {!! $item->note !!}
                                </p> --}}
                            </div>
                            <div class="row bg-primary rounded-bottom mx-0">
                                <div class="col-12 text-center px-0">
                                    <a wire:click='DetailTourism({{ $item->id }})' href="#"
                                        class="btn-hover btn text-white py-2 px-4"><i class="fas fa-eye"></i>
                                        ລາຍລະອຽດ</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- Packages End -->
    <!-- ວັດທະນາທຳ -->
    <div class="container-fluid packages pb-3">
        <div class="container py-3">
            <div class="mx-auto text-center mb-5" style="max-width: 900px;">
                <h5 class="section-title px-3">ແຫຼ່ງທ່ອງທ່ຽວ</h5>
                <h1 style="width: auto" class="mb-0">{{ $this->tourism_type_cultures->name_la }}</h1>
            </div>
            <div class="packages-carousel owl-carousel">
                @foreach ($tourism_type_culture as $item)
                    <div class="packages-item">
                        <div class="packages-img">
                            <img src="{{ asset($item->image) }}" class="img-fluid w-100 rounded-top" alt="Image" style="width: 100%; height:450px">
                            <div class="packages-info d-flex border border-start-0 border-end-0 position-absolute"
                                style="width: 100%; bottom: 0; left: 0; z-index: 5;">
                                <small class="flex-fill text-center border-end py-2"><i
                                        class="fa fa-map-marker-alt me-2"></i>
                                    @if (!empty($item->village))
                                        {{ $item->village->name_la }}-
                                    @endif
                                    @if (!empty($item->district))
                                        {{ $item->district->name_la }}-
                                    @endif
                                    @if (!empty($item->province))
                                        {{ $item->province->name_la }}
                                    @endif
                                </small>
                            </div>
                            <div class="packages-price py-2 px-4">
                                @if (!empty($item->tourism_type))
                                    {{ $item->tourism_type->name_la }}
                                @endif
                            </div>
                        </div>
                        <div class="packages-content bg-light">
                            <div class="p-4 pb-0">
                                <h5 class="mb-0">{{ $item->name }}</h5>
                                <small class="text-uppercase">ເຜີຍແຜ່ເເລ້ວ:
                                    {{ $item->created_at->diffForHumans() }}</small>
                                <div class="mb-3">
                                    <small class="text-primary"><i class="fas fa-eye"></i> ຍອດວິວເຂົ້າຊົມ 
                                        @if($item->viewer)
                                            {{ $item->viewer }}
                                            @else
                                            0
                                        @endif
                                        ຄັ້ງ</small>
                                </div>
                                {{-- <p class="mb-4">
                                    {!! $item->note !!}
                                </p> --}}
                            </div>
                            <div class="row bg-primary rounded-bottom mx-0">
                                <div class="col-12 text-center px-0">
                                    <a wire:click='DetailTourism({{ $item->id }})' href="#"
                                        class="btn-hover btn text-white py-2 px-4"><i class="fas fa-eye"></i>
                                        ລາຍລະອຽດ</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- Packages End -->

</div>
