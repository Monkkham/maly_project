<div wire:poll>

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h5><i class="fas fa-bullhorn"></i> ລາຍການເຜີຍແຜ່ແຫຼ່ງທ່ອງທ່ຽວ</h5>
                </div>
            </div>
        </div>
    </section>
    @foreach ($function_available as $item1)
    @if ($item1->function->name == 'action_10')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-header bg-light">
                            <div class="row mb-2">
                                <div class="col-sm-4">
                                    <h4 class="text-bold">ທັງຫມົດ: 
                                        {{ $count_qty }} ແຫ່ງ</h4>
                                </div>
                                <div wire:ignore class="col-sm-4">
                                    <div class="input-group row">
                                        <select wire:model='tourism_type_id' id="tourism_type_id"
                                            class="form-control form-control-md">
                                            <option class="text-left" selected value="">-- ປະເພດແຫຼ່ງທ່ອງທ່ຽວ --</option>
                                            @foreach ($tourism_type as $item)
                                                <option class="text-left" value="{{ $item->id }}">
                                                    {{ $item->name_la }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="input-group row">
                                        <input wire:model='search' type="search" class="form-control form-control-md"
                                            placeholder="ຊອກຫາ...">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-md btn-success">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <!-- Start Body -->
                            <div class="row">
                                <!-- Start Row -->
                                @foreach ($data as $item)
                                    <!-- Start Col -->
                                    <div class="col-md-2 d-flex align-items-stretch flex-column">
                                        {{-- @if ($item->stock == 0)
                                            <div class="ribbon-wrapper">
                                                <div class="ribbon bg-danger text-sm">
                                                    ຫມົດເເລ້ວ!
                                                </div>
                                            </div>
                                        @elseif($item->stock <= 10)
                                            <div class="ribbon-wrapper ribbon-sm">
                                                <div class="ribbon bg-warning text-sm">
                                                    ໃກ້ຫມົດ!
                                                </div>
                                            </div>
                                        @elseif($item->stock > 10)
                                            <div class="ribbon-wrapper ribbon-sm">
                                                <div class="ribbon bg-success text-sm">
                                                   ໃນສະຕ໋ອກ
                                                </div>
                                            </div>
                                        @endif --}}
                                        <div class="card bg-light d-flex flex-fill">
                                            <div class="card-header border-bottom-0">
                                                <b> {{ $item->name }} </b></p>
                                            </div>
                                            <div class="card-body pt-0">
                                                <div class="row text-center">
                                                    <div class="col-md-12">
                                                        @if (!empty($item))
                                                            <img class="rounded" src="{{ asset($item->image) }}"
                                                                width="150px;" height="150px;">
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h2 class="lead"><b></b></h2>
                                                        <p style="line-height: 20%;" class="text-muted text-sm">
                                                            @if (!empty($item->tourism_type))
                                                                {{ $item->tourism_type->name_la }}
                                                            @endif
                                                        </p>
                                                        <p style="line-height: 20%;" class="text-muted text-sm">
                                                            <b>ທີ່ຢູ່: </b>
                                                            @if (!empty($item))
                                                                {{ $item->village->name_la }},
                                                                {{ $item->district->name_la }},
                                                                {{ $item->province->name_la }}
                                                            @endif
                                                        </p>

                                                        {{-- @endif --}}
                                                    </div>
                                                </div>
                                                <div class="btn-group col-md-12">
                                                        {{-- <button wire:click='AddToCart({{ $item->id }})'
                                                            class="btn btn-info btn-sm float-right">
                                                            <i class="fas fa-eye"></i>
                                                        </button> --}}
                                                        <button wire:click='GoUpdatePublicPost({{ $item->id }})'
                                                            class="btn btn-warning btn-sm float-right">
                                                            <i class="fas fa-pen"></i>
                                                        </button>
                                                        <button wire:click='showDestory({{ $item->id }})'
                                                            class="btn btn-danger btn-sm float-right">
                                                            <i class="fas fa-trash"></i>
                                                        </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Col -->
                                @endforeach
                            </div>
                            <div class="float-right">
                                {{ $data->links() }}
                            </div>
                        </div>
                        <!-- /.card-body -->
                        {{-- <div class="card-body">
                            <div class="row">
                                @if ($lands == null)
                                    <div class="col-12">
                                        <div class="alert alert-danger">
                                            <h5><i class="icon fas fa-ban"></i> ບໍ່ມີຂໍ້ມູນທີ່ທ່ານຄົ້ນຫາ</h5>
                                        </div>
                                    </div>
                                @else
                                    @foreach ($lands as $items)
                                        <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch flex-column">
                                            <div class="card bg-light d-flex flex-fill">
                                                <div class="card-body">
                                                    <img src="https://thumbs.dreamstime.com/b/land-plot-aerial-view-development-investment-identify-registration-symbol-vacant-area-map-property-real-estate-218218332.jpg"
                                                        class="img-fluid rounded" width="100%" height="100%"
                                                        alt="jacksainther">
                                                </div>
                                                <div class="card-header text-muted border-bottom-0">
                                                    {{ $items->code }}
                                                    <small>ໂຊນ:
                                                        @if (!empty($items->zomes))
                                                        {{ $items->zomes->zname }}
                                                        @endif
                                                    </small>
                                                    <small>{{ number_format($items->selling_price) }} ₭</small>
                                                    @if ($cartData->where('id', $items->id)->count() > 0)
                                                        <button class="btn btn-warning btn-sm float-right">
                                                            <p class="mb-0">In Cart</p>
                                                        </button>
                                                    @else
                                                        <button
                                                            wire:click='addCart({{ $items->id }}, {{ $items->zones_id }}, {{ $items->zones->buy_lands_id }})'
                                                            class="btn btn-success btn-sm float-right">
                                                            <i class="fas fa-cart-plus"></i>
                                                        </button>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div> --}}
                        {{-- <div class="card-footer clearfix">
                            {{ $lands->links() }}
                        </div> --}}
                    </div>
                </div>

            </div>
    </section>
    @endif
    @endforeach
        {{-- ======== delete ======== --}}
        <div wire:ignore.self class="modal fabe" id="modal-delete">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <h4 class="modal-title"><i class="fa fa-trash"> </i> ລຶບອອກ</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h3 class="text-center">ທ່ານຕ້ອງການລຶບຂໍ້ມູນນີ້ອອກບໍ່?</h3>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">ຍົກເລີກ</button>
                        <button wire:click="destroy({{ $ID }})" type="button" class="btn btn-success"><i
                                class="fa fa-trash"></i> ລຶບອອກ</button>
                    </div>
                </div>
            </div>
        </div>
    @include('livewire.backend.data-store.modal-script')
</div>
