<div>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h5><i class="fas fa-bullhorn"></i> ແກ້ໄຂເຜີຍແຜ່ແຫຼ່ງທ່ອງທ່ຽວ <i class="fa fa-angle-double-right"></i>
                        ຟອມແກ້ໄຂເຜີຍແຜ່ຂໍ້ມູນແຫຼ່ງທ່ອງທ່ຽວ</h5>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('backend.dashboard') }}">ໜ້າຫຼັກ</a></li>
                        <li class="breadcrumb-item active">ຟອມເຜີຍແຜ່ຂໍ້ມູນແຫຼ່ງທ່ອງທ່ຽວ</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-warning text-center">
                            <h5><b><i class="fas fa-pen"></i> ແກ້ໄຂຂໍ້ມູນແຫຼ່ງທ່ອງທ່ຽວ</b></h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="container">
                                    <div wire:ignore class="avatar-upload">
                                        <div class="avatar-edit">
                                            <input type='file' wire:model="image" id="imageUpload"
                                                accept=".png, .jpg, .jpeg" />
                                            <label for="imageUpload"></label>
                                        </div>
                                        @error('image')
                                            <span style="color: red" class="text-danger">{{ $message }}</span>
                                        @enderror
                                        <label class="text-center">ປ່ຽນຮູບພາບຫຼັກ</label>
                                        <div class="avatar-preview">
                                            <div id="imagePreview"
                                                style="background-image: url({{ asset('logo/noimage.jpg') }});">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">ຊື່ຫົວຂໍ້</label>
                                        <input type="text" wire:model="name"
                                            class="form-control @error('name') is-invalid @enderror"
                                            placeholder="ປ້ອນຂໍ້ມູນ">
                                        @error('name')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for=""><span class="text-danger"></span>
                                            ປະເພດແຫຼ່ງທ່ອງທ່ຽວ</label>
                                        <select class="form-control" wire:model.live="tourism_type_id"
                                            id="tourism_type_id">
                                            <option value="">ເລືອກຂໍ້ມູນ</option>
                                            @foreach ($tourism_type as $item)
                                                <option value="{{ $item->id }}">
                                                    {{ $item->name_la }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @error('tourism_type_id')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="start_lat_long">ລະຫັດແຜນທີ່ຢູ່ຂອງສະຖານທີ່ {{ $this->latitude }}
                                            {{ $this->longitude }}</label>
                                        <input type="text" wire:model="start_lat_long"
                                            class="form-control @error('start_lat_long') is-invalid @enderror"
                                            placeholder="ປ້ອນຂໍ້ມູນ">
                                        @error('start_lat_long')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div wire:ignore.self class="form-group">
                                        <label for=""><span class="text-danger"></span>
                                            ແຂວງ</label>
                                        <select class="form-control" wire:model.live="province_id" id="province_id">
                                            <option value="">ເລືອກຂໍ້ມູນ</option>
                                            @foreach ($provinces as $item)
                                                <option value="{{ $item->id }}">
                                                    {{-- @if (Config::get('app.locale') == 'lo') --}}
                                                    {{ $item->name_la }}
                                                    {{-- @elseif(Config::get('app.locale') == 'en')
                                                            {{ $item->name_en }}
                                                        @elseif(Config::get('app.locale') == 'cn')
                                                            {{ $item->name_cn }}
                                                        @endif --}}
                                                </option>
                                            @endforeach
                                        </select>
                                        @error('province_id')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div wire:ignore.self class="form-group">
                                        <label for=""><span class="text-danger"></span>
                                            ເມືອງ</label>
                                        <select class="form-control" wire:model.live="district_id" id="district_id">
                                            <option value="">ເລືອກຂໍ້ມູນ</option>
                                            @foreach ($districts as $item)
                                                <option value="{{ $item->id }}">
                                                    {{-- @if (Config::get('app.locale') == 'lo') --}}
                                                    {{ $item->name_la }}
                                                    {{-- @elseif(Config::get('app.locale') == 'en')
                                                            {{ $item->name_en }}
                                                        @elseif(Config::get('app.locale') == 'cn')
                                                            {{ $item->name_cn }}
                                                        @endif --}}
                                                </option>
                                            @endforeach
                                        </select>
                                        @error('district_id')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div wire:ignore.self class="form-group">
                                        <label for=""><span class="text-danger"></span>
                                            ບ້ານ</label>
                                        <select class="form-control" wire:model.live="village_id" id="village_id">
                                            <option value="">ເລືອກຂໍ້ມູນ</option>
                                            @foreach ($villages as $item)
                                                <option value="{{ $item->id }}">
                                                    {{-- @if (Config::get('app.locale') == 'lo') --}}
                                                    {{ $item->name_la }}
                                                    {{-- @elseif(Config::get('app.locale') == 'en')
                                                            {{ $item->name_en }}
                                                        @elseif(Config::get('app.locale') == 'cn')
                                                            {{ $item->name_cn }}
                                                        @endif --}}
                                                </option>
                                            @endforeach
                                        </select>
                                        @error('village_id')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="detail">ລາຍລະອຽດເຜີຍແຜ່</label>
                                        <div wire:ignore>
                                            <textarea class="form-control" id="note" wire:model="note">{{ $note }}</textarea>
                                        </div>
                                        @error('note')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <!-- /.row -->
                                <div class="col-md-12">
                                    <div class="card card-default">
                                        <div class="card-header">
                                            <h3 class="card-title">ອັບໂຫລດຮູບພາບປະກອບ</h3>
                                        </div>
                                        <div class="card-body">
                                            <div id="actions" class="row">
                                                <div class="col-lg-6">
                                                    <div class="btn-group w-100">
                                                        <span class="btn btn-success col">
                                                            <input type="file" maxlength="5"
                                                                id="fileinput-button"
                                                                class="btn btn-success col fileinput-button"
                                                                style="padding: 3px; font-size: 12px;"
                                                                wire:model.live="images" multiple />
                                                            {{-- <i class="fas fa-plus"></i>
                                                        <span>ອັບຮູບພາບ</span> --}}
                                                        </span>
                                                        {{-- <button wire:click='resetField' type="reset" class="btn btn-warning col cancel">
                                                        <i class="fas fa-times-circle"></i>
                                                        <span>ຍົກເລີກທັງຫມົດ</span>
                                                    </button> --}}
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 d-flex align-items-center">
                                                    <div class="fileupload-process w-100">
                                                        <div id="total-progress"
                                                            class="progress progress-striped active"
                                                            role="progressbar" aria-valuemin="0" aria-valuemax="100"
                                                            aria-valuenow="0">
                                                            <div class="progress-bar progress-bar-success"
                                                                style="width:0%;" data-dz-uploadprogress></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @if (!empty($images->tourism_images))
                                                @foreach ($images->tourism_images as $item)
                                                    <span>
                                                        <img style="width: 100px; height:100px; padding:10px"
                                                            src="{{ asset($item->photo) }}" alt="Image">
                                                        <button wire:click='deleteImage({{ $item->id }})'
                                                            class="btn btn fas fa-times-circle text-danger"></button>
                                                    </span>
                                                @endforeach
                                            @endif
                                            <div class="table table-striped files" id="previews">
                                                <div id="template" class="row mt-2">
                                                    <div class="col-auto">
                                                        <span class="preview"><img src="data:," alt=""
                                                                data-dz-thumbnail /></span>
                                                    </div>
                                                    <div class="col d-flex align-items-center">
                                                        <p class="mb-0">
                                                            <span class="lead" data-dz-name></span>
                                                            (<span data-dz-size></span>)
                                                        </p>
                                                        <strong class="error text-danger"
                                                            data-dz-errormessage></strong>
                                                    </div>
                                                    <div class="col-4 d-flex align-items-center">
                                                        <div class="progress progress-striped active w-100"
                                                            role="progressbar" aria-valuemin="0" aria-valuemax="100"
                                                            aria-valuenow="0">
                                                            <div class="progress-bar progress-bar-success"
                                                                style="width:0%;" data-dz-uploadprogress></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-auto d-flex align-items-center">
                                                        <div class="btn-group">
                                                            <button style="display: none"
                                                                class="btn btn-primary start">
                                                                <i class="fas fa-upload"></i>
                                                                <span>ອັບໂຫຼດ</span>
                                                            </button>
                                                            <button style="display: none" data-dz-remove
                                                                class="btn btn-warning cancel">
                                                                <i class="fas fa-times-circle"></i>
                                                                <span>ຍົກເລີກ</span>
                                                            </button>
                                                            <button data-dz-remove class="btn btn-danger delete">
                                                                <i class="fas fa-trash"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.card -->
                                </div>
                                <!-- /.row -->
                                {{-- <div class="form-group">
                                    <label for="">ຮູບພາບປະກອບ</label>
                                    <input type="file" maxlength="4" class="form-control"
                                        style="padding: 3px; font-size: 12px;" wire:model="images" multiple />
                                    @error('images')
                                        <span style="color: red" class="text-danger"></span>
                                    @enderror
                                    @error('images.*')
                                        <span class="error">{{ $message }}</span>
                                    @enderror
                                    @error('images')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div> --}}
                                {{-- <div class="col-md-12" wire:ignore>
                                    <div id="map-update-s" style="height:250px; width: 100%;" class="my-3"></div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>ສະເເດງແຜນທີ່</label>
                                        <button type="button" class="btn btn-outline-primary form-control"><a
                                                href="#" onclick="getLocations()"><i
                                                    class="fas fa-map-marker-alt"></i> ສະເເດງແຜນທີ່ <i
                                                    class="icon-long-arrow-right"></i></a></button>
                                    </div>
                                </div> --}}
                                {{-- <div class="col-md-6">
                                        <div class="form-group">
                                            <label>lat</label>
                                            <input wire:model="lats" placeholder="lat" type="text"
                                                class="form-control @error('lats') is-invalid @enderror" id="lats" readonly>
                                            @error('lats')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>long</label>
                                            <input wire:model="longs" placeholder="long" type="text"
                                                class="form-control @error('longs') is-invalid @enderror" id="longs" readonly>
                                            @error('longs')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div> --}}
                            </div>
                        </div>

                        <div class="card-footer">
                            {{-- @foreach ($rolepermissions as $items)
                                @if ($items->permissionname->name == 'action_about') --}}
                            <a href="{{ route('backend.dashboard') }}" class="btn btn-info"><i
                                    class="fas fa-arrow-left"></i> ກັບຄືນຫນ້າຫຼັກ</a>
                            <button wire:click="Update()" class="btn btn-warning float-right"><i
                                    class="fas fa-check-circle"></i> ແກ້ໄຂເຜີຍແຜ່</button>
                            {{-- @endif
                                @endforeach --}}
                        </div>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </section>
</div>
@include('livewire.backend.data-store.office-script')

@push('scripts')
    <script>
        $(function() {
            //Initialize Select2 Elements
            $(".select2").select2();

            //Initialize Select2 Elements
            $(".select2bs4").select2({
                theme: "bootstrap4",
            });

            //Datemask dd/mm/yyyy
            $("#datemask").inputmask("dd/mm/yyyy", {
                placeholder: "dd/mm/yyyy",
            });
            //Datemask2 mm/dd/yyyy
            $("#datemask2").inputmask("mm/dd/yyyy", {
                placeholder: "mm/dd/yyyy",
            });
            //Money Euro
            $("[data-mask]").inputmask();

            //Date picker
            $("#reservationdate").datetimepicker({
                format: "L",
            });

            //Date and time picker
            $("#reservationdatetime").datetimepicker({
                icons: {
                    time: "far fa-clock",
                },
            });

            //Date range picker
            $("#reservation").daterangepicker();
            //Date range picker with time picker
            $("#reservationtime").daterangepicker({
                timePicker: true,
                timePickerIncrement: 30,
                locale: {
                    format: "MM/DD/YYYY hh:mm A",
                },
            });
            //Date range as a button
            $("#daterange-btn").daterangepicker({
                    ranges: {
                        Today: [moment(), moment()],
                        Yesterday: [
                            moment().subtract(1, "days"),
                            moment().subtract(1, "days"),
                        ],
                        "Last 7 Days": [moment().subtract(6, "days"), moment()],
                        "Last 30 Days": [moment().subtract(29, "days"), moment()],
                        "This Month": [
                            moment().startOf("month"),
                            moment().endOf("month"),
                        ],
                        "Last Month": [
                            moment().subtract(1, "month").startOf("month"),
                            moment().subtract(1, "month").endOf("month"),
                        ],
                    },
                    startDate: moment().subtract(29, "days"),
                    endDate: moment(),
                },
                function(start, end) {
                    $("#reportrange span").html(
                        start.format("MMMM D, YYYY") + " - " + end.format("MMMM D, YYYY")
                    );
                }
            );

            //Timepicker
            $("#timepicker").datetimepicker({
                format: "LT",
            });

            //Bootstrap Duallistbox
            $(".duallistbox").bootstrapDualListbox();

            //Colorpicker
            $(".my-colorpicker1").colorpicker();
            //color picker with addon
            $(".my-colorpicker2").colorpicker();

            $(".my-colorpicker2").on("colorpickerChange", function(event) {
                $(".my-colorpicker2 .fa-square").css("color", event.color.toString());
            });

            $("input[data-bootstrap-switch]").each(function() {
                $(this).bootstrapSwitch("state", $(this).prop("checked"));
            });
        });
        // BS-Stepper Init
        document.addEventListener("DOMContentLoaded", function() {
            window.stepper = new Stepper(document.querySelector(".bs-stepper"));
        });

        // DropzoneJS Demo Code Start
        Dropzone.autoDiscover = false;

        // Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
        var previewNode = document.querySelector("#template");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        var myDropzone = new Dropzone(document.body, {
            // Make the whole body a dropzone
            url: "/target-url", // Set the url
            thumbnailWidth: 80,
            thumbnailHeight: 80,
            parallelUploads: 20,
            previewTemplate: previewTemplate,
            autoQueue: false, // Make sure the files aren't queued until manually added
            previewsContainer: "#previews", // Define the container to display the previews
            clickable: ".fileinput-button", // Define the element that should be used as click trigger to select files.
        });

        myDropzone.on("addedfile", function(file) {
            // Hookup the start button
            file.previewElement.querySelector(".start").onclick = function() {
                myDropzone.enqueueFile(file);
            };
        });

        // Update the total progress bar
        myDropzone.on("totaluploadprogress", function(progress) {
            document.querySelector("#total-progress .progress-bar").style.width =
                progress + "%";
        });

        myDropzone.on("sending", function(file) {
            // Show the total progress bar when upload starts
            document.querySelector("#total-progress").style.opacity = "1";
            // And disable the start button
            file.previewElement
                .querySelector(".start")
                .setAttribute("disabled", "disabled");
        });

        // Hide the total progress bar when nothing's uploading anymore
        myDropzone.on("queuecomplete", function(progress) {
            document.querySelector("#total-progress").style.opacity = "0";
        });

        // Setup the buttons for all transfers
        // The "add files" button doesn't need to be setup because the config
        // `clickable` has already been specified.
        document.querySelector("#actions .start").onclick = function() {
            myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED));
        };
        document.querySelector("#actions .cancel").onclick = function() {
            myDropzone.removeAllFiles(true);
        };
        // DropzoneJS Demo Code End
    </script>
@endpush
