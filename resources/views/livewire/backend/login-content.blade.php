{{-- <div class="float-lg-start"><h4>{{ __('lang.headding1') }}</h4></div>
<div class="float-lg-start"><h4>{{ __('lang.headding2') }}</h4></div><br>
<marquee scrollamount="15" direction="left"> <h1>{{ __('lang.headding1') }} {{ __('lang.headding2') }}</h1> </marquee>
<div class="login-box">
    <marquee scrollamount="12" direction="left" class="text-white">
        <h1><i class="flag-icon flag-icon-la"></i> ສາທາລະນະລັດ ປະຊາທິປະໄຕ ປະຊາຊົນລາວ ສັນຕິພາບ ເອກະລາດ ປະຊາທິປະໄຕ ເອກະພາບ ວັດທະນາຖາວອນ</h1>
    </marquee>
    <div class="card card-outline card-primary">
        <div class="card-header text-center">
            <a href="#" class="h2"><i class="fa fa-user-tie"></i> ຍິນດີຕ້ອນຮັບທ່ານ</a>
        </div>
        <div class="card-body">
            <p class="login-box-msg h5">
                <a href="#" class="brand-link">
                    <img src="{{ asset('logo/logo.jpg') }}" class="img-circle elevation-2" height="70"> <br>
                    <a href="#" target="_blank" class="h5"><b>ຊື່ລະບົບ</b></a>
                </a>
            </p>
            <div class="input-group mb-3">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-phone"></span>
                    </div>
                </div>
                <input type="tel" wire:model="phone" wire:keydown.enter="login"
                 class="form-control @error('phone') is-invalid @enderror"
                    placeholder="ເບີໂທ">

            </div>
            @error('phone')
                <span style="color: red" class="error">{{ $message }}</span>
            @enderror
            <div class="input-group mb-3">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                    </div>
                </div>
                <input type="password" wire:model="password" wire:keydown.enter="login"
                    class="form-control @error('password') is-invalid @enderror"
                    placeholder="ລະຫັດຜ່ານ">

            </div>
            @error('password')
                <span style="color: red" class="error">{{ $message }}</span>
            @enderror
            <div class="row">
                <div class="col-8">
              <div class="icheck-primary">
                <input type="checkbox" id="remember" wire:model="remember">
                <label for="remember">
                  ຈຶ່ຈຳຂ້ອຍໄວ້
                </label>
              </div>
            </div>
            </div>
            <div class="social-auth-links text-center mt-2 mb-3">
                <button wire:click="login" class="btn btn-block btn-primary">
                    <i class="fa fa-sign-in-alt"></i> ເຂົ້າສູ່ລະບົບ
                </button>
            </div>
            </form>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<!-- /.login-box --> --}}


<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-4">
                <div class="login-wrap p-0">
                    <h2 class="text-center text-white text-bold"><i class="fa fa-lock"></i> ເຂົ້າສູ່ລະບົບ</h2>
                    <form class="signin-form">
                        <div class="form-group">
                            <input type="number" wire:model="phone" wire:keydown.enter="login"
                                class="form-control @error('phone') is-invalid @enderror" placeholder="ເບີໂທ">
                        </div>
                        @error('phone')
                            <span style="color: red" class="error">{{ $message }}</span>
                        @enderror
                        <div class="form-group">
                            <input id="password-field" wire:model="password" wire:keydown.enter="login" type="password"
                                class="form-control @error('password') is-invalid @enderror" placeholder="ລະຫັດຜ່ານ">
                            <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                        </div>
                        @error('password')
                            <span style="color: red" class="error">{{ $message }}</span>
                        @enderror
                        <div class="form-group">
                            <button type="button" wire:click="login" class="form-control btn btn-primary bg-info submit px-3 text-white"><i class="fa fa-sign-in"></i> ເຂົ້າສູ່ລະບົບ</button>
                        </div>
                        <div class="form-group d-md-flex">
                            <div class="w-50">
                                <label class="checkbox-wrap checkbox-primary">ຈື່ຈຳຂ້ອຍໄວ້
                                    <input type="checkbox" id="remember" wire:model="remember">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            {{-- <div class="w-50 text-md-right">
                                <a href="#" style="color: #fff">Forgot Password</a>
                            </div> --}}
                        </div>
                    </form>
                    {{-- <p class="w-100 text-center">&mdash; Or Sign In With &mdash;</p>
                    <div class="social d-flex text-center">
                        <a href="#" class="px-2 py-2 mr-md-1 rounded"><span class="ion-logo-facebook mr-2"></span>
                            Facebook</a>
                        <a href="#" class="px-2 py-2 ml-md-1 rounded"><span class="ion-logo-twitter mr-2"></span>
                            Twitter</a>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</section>
