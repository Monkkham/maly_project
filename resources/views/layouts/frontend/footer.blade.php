        <!-- Footer Start -->
        <div class="container-fluid footer py-2">
            <div class="container py-2">
                <div class="row g-5">
                    <div class="col-md-4">
                        <div class="footer-item d-flex flex-column">
                            <h4 class="mb-4 text-white"><i class="fas fa-map-marked-alt"></i> ທີ່ຕັ້ງ</h4>
                            @if (!empty($office))
                                <a href=""><i class="fas fa-home me-2"></i> {{ $office->address }}</a>
                                <a href=""><i class="fas fa-phone me-2"></i> {{ $office->phone }}</a>
                            @endif
                            <div class="d-flex align-items-center">
                                <i class="fas fa-share fa-2x text-white me-2"></i>
                                <a class="btn-square btn btn-primary rounded-circle mx-1" href=""><i
                                        class="fab fa-facebook-f"></i></a>
                                <a class="btn-square btn btn-primary rounded-circle mx-1" href=""><i
                                        class="fab fa-twitter"></i></a>
                                <a class="btn-square btn btn-primary rounded-circle mx-1" href=""><i
                                        class="fab fa-instagram"></i></a>
                                <a class="btn-square btn btn-primary rounded-circle mx-1" href=""><i
                                        class="fab fa-linkedin-in"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="footer-item d-flex flex-column">
                            <h4 class="mb-4 text-white"><i class="fas fa-list"></i> ເມນູລະບົບ</h4>
                            <a href="{{ route('frontend.home') }}"><i class="fas fa-angle-right me-2"></i> ໜ້າຫຼັກ</a>
                            <a href="{{ route('frontend.tourism') }}"><i class="fas fa-angle-right me-2"></i>
                                ແຫຼ່ງທ່ອງທ່ຽວ</a>
                            <a href="{{ route('frontend.about') }}"><i class="fas fa-angle-right me-2"></i> ກ່ຽວກັບ</a>
                            <a href="{{ route('frontend.contact') }}"><i class="fas fa-angle-right me-2"></i> ຕິດຕໍ່</a>
                            <a href="{{ route('frontend.signup') }}"><i class="fas fa-angle-right me-2"></i>
                                ລົງທະບຽນ</a>
                            <a href="{{ route('frontend.signout') }}"><i class="fas fa-angle-right me-2"></i>
                                ເຂົ້າສູ່ລະບົບ</a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="footer-item d-flex flex-column">
                            <h4 class="mb-4 text-white"><i class="fas fa-phone-alt"></i> ຊັບພອດ</h4>
                            <a href=""><i class="fas fa-angle-right me-2"></i> ຕິດຕໍ່:
                                @if (!empty($office))
                                <a href=""><i class="fas fa-phone me-2"></i> {{ $office->phone }}</a>
                            @endif</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer End -->
