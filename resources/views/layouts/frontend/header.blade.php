        <!-- Topbar Start -->
        <div class="container-fluid bg-primary px-5 d-none d-lg-block">
            <div class="row gx-0">
                <div class="col-lg-8 text-center text-lg-start mb-2 mb-lg-0">
                    <div class="d-inline-flex align-items-center" style="height: 45px;">
                        <a class="btn btn-sm btn-outline-light btn-sm-square rounded-circle me-2" href=""><i
                                class="fab fa-twitter fw-normal"></i></a>
                        <a class="btn btn-sm btn-outline-light btn-sm-square rounded-circle me-2" href=""><i
                                class="fab fa-facebook-f fw-normal"></i></a>
                        <a class="btn btn-sm btn-outline-light btn-sm-square rounded-circle me-2" href=""><i
                                class="fab fa-linkedin-in fw-normal"></i></a>
                        <a class="btn btn-sm btn-outline-light btn-sm-square rounded-circle me-2" href=""><i
                                class="fab fa-instagram fw-normal"></i></a>
                        <a class="btn btn-sm btn-outline-light btn-sm-square rounded-circle" href=""><i
                                class="fab fa-youtube fw-normal"></i></a>
                    </div>
                </div>
                <div class="col-lg-4 text-center text-lg-end">
                    <div class="d-inline-flex align-items-center" style="height: 45px;">
                            <a href="#"><small class="me-3 text-white"><i
                                        class="fas fa-phone-alt me-2 text-white"></i>
                                        @if (!empty($office))
                                         {{ $office->phone }}
                                    @endif</small></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Topbar End -->

        <!-- Navbar & Hero Start -->
        <div class="container-fluid position-relative p-0">
            <nav class="navbar navbar-expand-lg navbar-light px-4 px-lg-5 py-3 py-lg-0">
                <a href="{{ route('frontend.home') }}" class="navbar-brand p-0">
                    <h1 class="m-0"><img style="border-radius: 100%" src="{{ asset('logo/logo.jpg') }}"
                            alt="Logo"></h1>
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarCollapse">
                    <span class="fa fa-bars"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <div class="navbar-nav ms-auto py-0">
                        <a href="{{ route('frontend.home') }}" class="nav-item nav-link"><i class="fas fa-home"></i>
                            ໜ້າຫຼັກ</a>
                        <a href="{{ route('frontend.tourism') }}" class="nav-item nav-link"><i class="fas fa-globe"></i>
                            ແຫຼ່ງທ່ອງທ່ຽວ</a>
                        <a href="{{ route('frontend.about') }}" class="nav-item nav-link"><i
                                class="fas fa-address-book"></i>
                            ກ່ຽວກັບ</a>
                        {{-- <div class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Pages</a>
                            <div class="dropdown-menu m-0">
                                <a href="destination.html" class="dropdown-item">Destination</a>
                                <a href="tour.html" class="dropdown-item">Explore Tour</a>
                            </div>
                        </div> --}}
                        <a href="{{ route('frontend.contact') }}" class="nav-item nav-link"><i
                                class="fas fa-phone-alt"></i> ຕິດຕໍ່</a>
                        {{-- @auth
                        @else
                            <a href="{{ route('frontend.signup') }}" class="nav-item nav-link"><i class="fas fa-user"></i>
                                ລົງທະບຽນ</a>
                        @endauth --}}
                    </div>
                    <a href="#" class="btn btn-primary rounded-pill py-2 px-4 ms-lg-4"><i
                        class="fas fa-eye text-white"></i>
                        ຍອດເຂົ້າຊົມ
                        @if(!empty($visitorCount))
                            {{ $visitorCount }}
                        @endif
                          ຄັ້ງ</a>
                    {{-- @auth
                        <a href="{{ route('frontend.profile') }}" class="btn btn-primary rounded-pill py-2 px-4 ms-lg-4"><i
                                class="fas fa-user-alt text-white"></i> {{ auth()->user()->name_lastname }}</a>
                        <a href="{{ route('frontend.signout') }}" class="btn btn-danger rounded-pill py-2 px-4 ms-lg-4"><i
                                class="fas fa-sign-in-alt text-white"></i> ອອກລະບົບ</a>
                    @else
                        <a href="{{ route('frontend.signin') }}" class="btn btn-primary rounded-pill py-2 px-4 ms-lg-4"><i
                                class="fas fa-lock text-white"></i> ເຂົ້າສູ່ລະບົບ</a>
                    @endauth --}}
                </div>
            </nav>
        </div>
        <!-- Navbar & Hero End -->
