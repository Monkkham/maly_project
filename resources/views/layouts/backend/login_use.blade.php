<!doctype html>
<html lang="en">

<head>
    <title>Login 10</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="{{ asset('login_use/css/style.css') }}">

    <!-- sweetalert2 -->
    <link rel="stylesheet" href="{{ asset('backend/plugins/sweetalert2/sweetalert2.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/plugins/sweetalert2/sweetalert2.min.css') }}">
    @livewireStyles

</head>

<body class="img js-fullheight" style="background-image: url(login_use/images/login.jpg);">
    {{ $slot }}

    <script src="{{ asset('login_use/js/jquery.min.js') }}"></script>
    <script src="{{ asset('login_use/js/popper.js') }}"></script>
    <script src="{{ asset('login_use/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('login_use/js/main.js') }}"></script>

    <script src="{{ asset('backend/plugins/sweetalert2/sweetalert2.all.js') }}"></script>
    <script src="{{ asset('backend/plugins/sweetalert2/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/sweetalert2/sweetalert2.js') }}"></script>
    <script src="{{ asset('backend/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    @livewireScripts
    @include('layouts.backend.script')
    @stack('scripts')
</body>

</html>
