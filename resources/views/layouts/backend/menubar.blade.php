<nav class="main-header navbar navbar-expand-md navbar-light navbar-white"
    @if (request()->is('check-bill-restaurant')) style="position: sticky;top:0;" @endif>
    <div class="container-fluid">
        <a href="{{ route('backend.dashboard') }}" class="navbar-brand">
            <img src="{{ asset('logo/logo.jpg') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                style="opacity: .8">
            <span class="brand-text font-weight-light">
                @if (!empty($office->name_la))
                    {{ $office->name_la }}
                @endif
            </span>
        </a>
        <!-- Left navbar links -->
        <div class="collapse navbar-collapse order-3" id="navbarCollapse">

            <ul class="navbar-nav">
                {{-- ========= ຈັດການຂໍ້ມູນ ========== --}}
                @foreach ($function_available as $item1)
                    @if ($item1->function->name == 'action_1')
                        <li class="nav-item dropdown  text-bold">
                            <a id="dropdownSubMenu1" href="javascript:void(0)" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"><i
                                    class="fas fa-database"></i>
                                ຈັດການຂໍ້ມູນ</a>
                            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                                <li class="dropdown-submenu dropdown-hover">
                                    @foreach ($function_available as $item1)
                                        @if ($item1->function->name == 'action_6')
                                            <a id="dropdownSubMenu2" href="#" role="button"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                                                class="dropdown-item dropdown-toggle">
                                                <i class="fa fa-angle-double-right main-web-color"></i>
                                                ຂໍ້ມູນທີ່ຢູ່</a>
                                        @endif
                                    @endforeach
                                    <ul aria-labelledby="dropdownSubMenu2" class="dropdown-menu border-0 shadow">
                                        <li>
                                            <a tabindex="-1" href="{{ route('backend.village') }}"
                                                class="dropdown-item"><i
                                                    class="fa fa-angle-double-right main-web-color"></i>
                                                ບ້ານ</a>
                                        </li>
                                        <li>
                                            <a tabindex="-1" href="{{ route('backend.district') }}"
                                                class="dropdown-item"><i
                                                    class="fa fa-angle-double-right main-web-color"></i>
                                                ເມືອງ</a>
                                        </li>
                                        <li>
                                            <a tabindex="-1" href="{{ route('backend.province') }}"
                                                class="dropdown-item"><i
                                                    class="fa fa-angle-double-right main-web-color"></i>
                                                ແຂວງ</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown-divider"></li>
                                @foreach ($function_available as $item1)
                                    @if ($item1->function->name == 'action_7')
                                        <li><a href="{{ route('backend.user') }}" class="dropdown-item"><i
                                                    class="fa fa-angle-double-right main-web-color"></i>
                                                ຜູ້ໃຊ້</a>
                                        </li>
                                        <li class="dropdown-divider"></li>
                                    @endif
                                @endforeach
                                @foreach ($function_available as $item1)
                                    @if ($item1->function->name == 'action_8')
                                        <li><a href="{{ route('backend.role') }}" class="dropdown-item"><i
                                                    class="fa fa-angle-double-right main-web-color"></i>
                                                ສິດນຳໃຊ້</a>
                                        </li>
                                        <li class="dropdown-divider"></li>
                                    @endif
                                @endforeach
                                @foreach ($function_available as $item1)
                                    @if ($item1->function->name == 'action_9')
                                        <li><a href="{{ route('backend.TourismType') }}" class="dropdown-item"><i
                                                    class="fa fa-angle-double-right main-web-color"></i>
                                                ປະເພດແຫຼ່ງທ່ອງທ່ຽວ</a>
                                        <li class="dropdown-divider"></li>
                                    @endif
                                @endforeach
                                @foreach ($function_available as $item1)
                                    @if ($item1->function->name == 'action_10')
                                        <li><a href="{{ route('backend.Tourism') }}" class="dropdown-item"><i
                                                    class="fa fa-angle-double-right main-web-color"></i>
                                                ແຫຼ່ງທ່ອງທ່ຽວ</a>
                                        <li class="dropdown-divider"></li>
                                    @endif
                                @endforeach
                                @foreach ($function_available as $item1)
                                    @if ($item1->function->name == 'action_11')
                                        <li><a href="{{ route('backend.Office') }}" class="dropdown-item"><i
                                                    class="fa fa-angle-double-right main-web-color"></i>
                                                ຫ້ອງການ</a>
                                        </li>
                                    @endif
                                @endforeach
                        </li>
            </ul>
            </li>
            @endif
            @endforeach
            {{-- ============== income expend ============== --}}
            @foreach ($function_available as $item1)
                @if ($item1->function->name == 'action_12')
                    <li class="nav-item dropdown  text-bold">
                        <a id="dropdownSubMenu1" href="{{ route('backend.CreatePublicPostTourism') }}"
                            aria-haspopup="true" aria-expanded="false" class="nav-link">
                            <i class="fas fa-bullhorn"></i> ເຜີຍແຜ່ແຫຼ່ງທ່ອງທ່ຽວ
                        </a>
                    </li>
                @endif
            @endforeach
            {{-- ============== reports ============== --}}
            <li class="nav-item dropdown  text-bold">
                @foreach ($function_available as $item1)
                    @if ($item1->function->name == 'action_3')
                        <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false" class="nav-link dropdown-toggle">
                            <i class="fas fa-chart-line"></i> ລາຍງານ
                        </a>
                    @endif
                @endforeach
                <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                    @foreach ($function_available as $item1)
                        @if ($item1->function->name == 'action_12')
                            <li>
                                <a href="{{ route('backend.ReportTourism') }}" class="dropdown-item"><i
                                        class="fa fa-angle-double-right main-web-color"></i>
                                    ເຜີຍແຜ່ແຫຼ່ງທ່ອງທ່ຽວ
                                </a>
                            </li>
                        @endif
                    @endforeach
                    @foreach ($function_available as $item1)
                        @if ($item1->function->name == 'action_13')
                            <li>
                                <a href="{{ route('backend.ReportTourismPlace') }}" class="dropdown-item"><i
                                        class="fa fa-angle-double-right main-web-color"></i>
                                    ສະຖານທີຼ່ແຫຼ່ງທ່ອງທ່ຽວ
                                </a>
                            </li>
                        @endif
                    @endforeach
                    @foreach ($function_available as $item1)
                        @if ($item1->function->name == 'action_14')
                            <li>
                                <a href="{{ route('backend.ReportUser') }}" class="dropdown-item"><i
                                        class="fa fa-angle-double-right main-web-color"></i>
                                    ຂໍ້ມູນຜູູ້ໃຊ້ລະບົບ
                                </a>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </li>
            @foreach ($function_available as $item1)
                @if ($item1->function->name == 'action_4')
                    <li class="nav-item dropdown  text-bold">
                        <a id="dropdownSubMenu1" href="{{ route('backend.Slide') }}" aria-haspopup="true"
                            aria-expanded="false" class="nav-link">
                            <i class="fas fa-images"></i> ສະໄລຮູບພາບ
                        </a>
                    </li>
                @endif
            @endforeach
        </div>
        <!-- Right navbar links -->
        <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
            <!-- Notifications Dropdown Menu -->
            {{-- <li class="nav-item dropdown  text-bold">
                <a class="nav-link" data-toggle="dropdown" href="">
                    <i class="fas fa-bell"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right p-0">
                    <a class="nav-link" data-toggle="nav-item" href="{{ url('localization/lo') }}">
                        <i class="flag-icon flag-icon-la"></i> {{ __('lang.lao') }}
                    </a>
                    <a class="nav-link" data-toggle="nav-item" href="{{ url('localization/en') }}">
                        <i class="flag-icon flag-icon-us"></i> {{ __('lang.english') }}
                    </a>
                    <a class="nav-link" data-toggle="nav-item" href="{{ url('localization/cn') }}">
                        <i class="flag-icon flag-icon-cn"></i> {{ __('lang.chinese') }}
                    </a>
                </div>
            </li> --}}
            <li class="nav-item dropdown  text-bold">
            <li class="nav-item dropdown  text-bold">
                @foreach ($function_available as $item1)
                @if ($item1->function->name == 'action_5')
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <img src="{{ asset('logo/logo.jpg') }}" style="width: 30px; height: 30px" alt="AdminLTE Logo"
                        class="brand-image img-circle elevation-3" style="opacity: .8">
                    {{ auth()->user()->name_lastname }}
                    <span class="brand-text font-weight-light text-md"></span>
                </a>
                @endif
                @endforeach
                <div class="dropdown-menu dropdown-menu-right p-0">
                    <a class="nav-link" data-toggle="nav-item" href="{{ route('backend.profile') }}">
                        <i class="fas fa-user-tie"></i> ໂປຣຟາຍ
                    </a>
                    <div class="dropdown-divider"></div>
                    <a data-toggle="modal" data-target="#modal-default" class="nav-link"
                        data-controlsidebar-slide="true" role="button">
                        <i class="fas fa-sign-out-alt text-danger"></i> ອອກຈາກລະບົບ
                    </a>
                </div>
            </li>
            <li class="user-footer">

            </li>
        </ul>
        <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse"
            aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
</nav>
{{-- =========================== modal logout ============================ --}}
<div class="modal fade" id="modal-default">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <p class="modal-title"> <i class="fas fa-sign-out-alt"></i>ອອກຈາກລະບົບ</p>
                <button type="button" class="close bg-dangerr" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5 class="text-center">ທ່ານຕ້ອງການອອກຈາກລະບົບບໍ່?</h5>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-danger" data-dismiss="modal">ຍົກເລີກ</button>
                <a type="button" href="{{ route('backend.logout') }}" class="btn btn-primary">ຕົກລົງ</a>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
