<?php

use App\Http\Livewire\Backend\CreatePublicPostTourismContent;
use App\Http\Livewire\Backend\DashboardContent;
use App\Http\Livewire\Backend\DataStore\OfficeContent;
use App\Http\Livewire\Backend\DataStore\TourismContent;
use App\Http\Livewire\Backend\DataStore\TourismTypeContent;
use App\Http\Livewire\Backend\DataStore\UserContent;
use App\Http\Livewire\Backend\DistrictContent;
use App\Http\Livewire\Backend\LoginContent;
use App\Http\Livewire\Backend\LogoutContent;
use App\Http\Livewire\Backend\ProfileContent;
use App\Http\Livewire\Backend\ProvinceContent;
use App\Http\Livewire\Backend\Report\ReportTourismContent;
use App\Http\Livewire\Backend\Report\ReportTourismPlaceContent;
use App\Http\Livewire\Backend\Report\ReportUsersContent;
use App\Http\Livewire\Backend\RolesContent;
use App\Http\Livewire\Backend\SlideContent;
use App\Http\Livewire\Backend\UpdatePublicPostTourismContent;
use App\Http\Livewire\Backend\VillageContent;
use App\Http\Livewire\Frontend\AboutContent;
use App\Http\Livewire\Frontend\ContactContent;
use App\Http\Livewire\Frontend\HomeContent;
use App\Http\Livewire\Frontend\ProfilesContent;
use App\Http\Livewire\Frontend\SearchContent;
use App\Http\Livewire\Frontend\SigninContent;
use App\Http\Livewire\Frontend\SignoutContent;
use App\Http\Livewire\Frontend\SignupContent;
use App\Http\Livewire\Frontend\TourismDetailContent;
use App\Http\Livewire\Frontend\TourismsContent;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
// ========== Frontend ====================================//
Route::get('/', HomeContent::class)->name('frontend.home');
Route::get('/abouts', AboutContent::class)->name('frontend.about');
Route::get('/contacts', ContactContent::class)->name('frontend.contact');
Route::get('/signins', SigninContent::class)->name('frontend.signin');
Route::get('/signups', SignupContent::class)->name('frontend.signup');
Route::get('/Searchs', SearchContent::class)->name('frontend.Search');

Route::get('/tourimss', TourismsContent::class)->name('frontend.tourism');
Route::get('/TourismDetailContents/{slug_id}', TourismDetailContent::class)->name('frontend.TourismDetailContent');
Route::group(['middleware' => 'auth.frontend'], function () {
    Route::get('/signouts', [SignoutContent::class, 'SignOut'])->name('frontend.signout');
    Route::get('/frontend-profiles', ProfilesContent::class)->name('frontend.profile');
   
});
// ========== Backend ====================================//
Route::get('/login-admin', LoginContent::class)->name('backend.login');
Route::group(['middleware' => 'auth.backend'], function () {
    Route::get('/logout', [LogoutContent::class, 'logout'])->name('backend.logout');
    Route::get('/dashboard', DashboardContent::class)->name('backend.dashboard');
    Route::get('/admin-profiles', ProfileContent::class)->name('backend.profile');
    Route::get('/users', UserContent::class)->name('backend.user');
    Route::get('/roles', RolesContent::class)->name('backend.role');
    Route::get('/villages', VillageContent::class)->name('backend.village');
    Route::get('/districts', DistrictContent::class)->name('backend.district');
    Route::get('/provinces', ProvinceContent::class)->name('backend.province');
    Route::get('/Offices', OfficeContent::class)->name('backend.Office');
    Route::get('/TourismTypes', TourismTypeContent::class)->name('backend.TourismType');
    Route::get('/Tourisms', TourismContent::class)->name('backend.Tourism');
    Route::get('/Slides', SlideContent::class)->name('backend.Slide');
    Route::get('/CreatePublicPostTourisms', CreatePublicPostTourismContent::class)->name('backend.CreatePublicPostTourism');
    Route::get('/UpdatePublicPostTourisms/{slug_id}', UpdatePublicPostTourismContent::class)->name('backend.UpdatePublicPostTourism');

    Route::get('/ReportTourisms', ReportTourismContent::class)->name('backend.ReportTourism');
    Route::get('/ReportTourismPlaces', ReportTourismPlaceContent::class)->name('backend.ReportTourismPlace');
    Route::get('/ReportUsers', ReportUsersContent::class)->name('backend.ReportUser');
});
