<?php

namespace App\Providers;

use App\Models\FunctionAvailable;
use App\Models\Office;
use App\Models\Visitor;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{


    public function storeVisitor()
    {
        // Save visitor information to database
        $visitor = new Visitor();
        $visitor->ip_address = Request::ip();
        // Add other fields as needed, e.g., user agent, timestamp, etc.
        $visitor->save();
    }
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', function ($view) {
            $this->storeVisitor();
            $visitorCount = Visitor::count();
            $office = Office::first();
            if (Auth::check()) {
                $office = Office::first();
                $function_available = FunctionAvailable::select('function_availables.*')
                    ->join('functions as f', 'f.id', '=', 'function_availables.function_id')
                    ->where('function_availables.role_id', auth()->user()->roles_id)
                    ->orderBy('f.id', 'ASC')->get();
                View::share(([
                    'office' => $office,
                    'function_available' => $function_available,
                ]));
            }
            View::share(([
                'office' => $office,
                'visitorCount' => $visitorCount,
            ]));
        });
    }
}
