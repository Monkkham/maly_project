<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImageMulti extends Model
{
    use HasFactory;
    protected $table = "tourism_images";
    protected $fillable = [
        'id',
        'public_post_tourism_id',
        'photo',
        'created_at',
        'updated_at'
    ];
    public function public_post_tourism()
    {
        return $this->belongsTo('App\Models\PublicPostTourism', 'public_post_tourism_id', 'id');
    }
}
