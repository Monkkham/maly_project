<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PublicPostTourism extends Model
{
    use HasFactory;
    protected $table = 'public_post_tourism';
    protected $fillable = [
        'id',
        'tourism_type_id',
        'users_id',
        'village_id',
        'district_id',
        'province_id',
        'name',
        'image',
        'note',
        'latitude',
        'longitude',
        'viewer',
        'created_at',
        'updated_at',
    ];
    public function tourism_type()
    {
        return $this->belongsTo('App\Models\TourismType','tourism_type_id','id');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User','users_id','id');
    }
    public function village()
    {
        return $this->belongsTo('App\Models\Village','village_id','id');
    }
    public function district()
    {
        return $this->belongsTo('App\Models\District', 'district_id', 'id');
    }
    public function province()
    {
        return $this->belongsTo('App\Models\Province','province_id','id');
    }
    public function tourism_images()
    {
        return $this->hasMany(ImageMulti::class);
    }
}
