<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TourismType extends Model
{
    use HasFactory;
    protected $table = 'tourism_type';
    protected $fillable = [
        'id',
        'name_la',
        'name_en',
        'created_at',
        'updated_at',
    ];
}
