<?php

namespace App\Http\Livewire\Backend\DataStore;

use App\Models\ImageMulti;
use App\Models\PublicPostTourism;
use App\Models\TourismType;
use Livewire\Component;

class TourismContent extends Component
{
    public $search, $tourism_type_id, $ID;
    public function render()
    {
        $tourism_type = TourismType::all();
        $data = PublicPostTourism::where(function ($q) {
            $q->where('name', 'like', '%' . $this->search . '%');
        });
        if ($this->tourism_type_id) {
            $data = $data->where('tourism_type_id', $this->tourism_type_id);
            $count_qty = $data->where('tourism_type_id', $this->tourism_type_id)->count();
        }
        if (!empty($data)) {
            $data = $data->paginate(18);
            $count_qty = $data->count();
        }
        return view('livewire.backend.data-store.tourism-content', compact('data', 'tourism_type', 'count_qty'))->layout('layouts.backend.style');
    }
    public function resetField()
    {
        $this->ID = '';
    }
    public function showDestory($ids)
    {
        $this->ID = $ids;
        $data = PublicPostTourism::find($ids);
        // $this->name = $data->name;
        $this->dispatchBrowserEvent('show-modal-delete');
    }

    public function destroy()
    {
        $tourism = PublicPostTourism::find($this->ID);

        if (!$tourism) {
            return;
        }

        $ImageMultis = ImageMulti::where('public_post_tourism_id', $this->ID)->get();
        foreach ($ImageMultis as $ImageMulti) {
            $ImageMulti->delete();
        }
        $tourism->delete();
        $this->resetField();

        $this->dispatchBrowserEvent('hide-modal-delete');
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ສຳເລັດເເລ້ວ!',
            'icon' => 'success',
        ]);
        return redirect(route('backend.Tourism'));
    }
    public function GoUpdatePublicPost($ids)
    {
        return redirect(route('backend.UpdatePublicPostTourism', $ids));
    }
}
