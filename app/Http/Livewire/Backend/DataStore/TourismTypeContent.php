<?php

namespace App\Http\Livewire\Backend\DataStore;

use App\Models\TourismType;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class TourismTypeContent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $ID, $name_la, $name_en, $search;
    public function mount()
    {
        $this->search = '';
    }
    public function render()
    {
        $data = TourismType::where(function ($q) {
            $q->where('name_la', 'like', '%' . $this->search . '%');
        })->paginate(5);
        return view('livewire.backend.data-store.tourism-type-content', compact('data'))->layout('layouts.backend.style');
    }
    public function resetform()
    {
        $this->name_la = '';
        $this->name_en = '';
        $this->ID = '';
    }
    protected $rules = [
        'name_la' => 'required|unique:tourism_type',
    ];
    protected $messages = [
        'name_la.required' => 'ປ້ອນຂໍ້ມູນກ່ອນ!',
        'name_la.unique' => 'ຂໍ້ມູນນີ້ມີໃນລະບົບເເລ້ວ!',
    ];
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }
    public function store()
    {
        $updateId = $this->ID;
        if ($updateId > 0) {
            $this->validate([
                'name_la' => 'required',
            ], [
                'name_la.required' => 'ປ້ອນຂໍ້ມູນກ່ອນ!',
            ]);
            $data = TourismType::find($updateId);
            $data->name_la = $this->name_la;
            $data->name_en = $this->name_en;
            $data->save();
            $this->dispatchBrowserEvent('swal', [
                'title' => 'ສຳເລັດເເລ້ວ!',
                'icon' => 'success',
            ]);
            $this->resetform();
        } else //ເພີ່ມໃໝ່
        {
            $this->validate([
                'name_la' => 'required|unique:tourism_type',
            ], [
                'name_la.required' => 'ປ້ອນຂໍ້ມູນກ່ອນ!',
                'name_la.unique' => 'ຂໍ້ມູນນີ້ມີໃນລະບົບເເລ້ວ!',
            ]);
            try {
                DB::beginTransaction();
                $data = new TourismType();
                $data->name_la = $this->name_la;
                $data->name_en = $this->name_en;
                $data->save();
                $this->dispatchBrowserEvent('swal', [
                    'title' => 'ສຳເລັດເເລ້ວ!',
                    'icon' => 'success',
                ]);
                $this->resetform();
                DB::commit();
            } catch (\Exception $ex) {
                DB::rollBack();
                $this->dispatchBrowserEvent('swal', [
                    'title' => 'ມີບາງຢ່າງຜິດພາດ!',
                    'icon' => 'warning',
                ]);
            }
        }
    }
    public function edit($ids)
    {
        $data = TourismType::find($ids);
        $this->name_la = $data->name_la;
        $this->name_en = $data->name_en;
        $this->ID = $data->id;
    }
    public function showDestroy($ids)
    {
        $this->dispatchBrowserEvent('show-modal-delete');
        $data = TourismType::find($ids);
        $this->ID = $data->id;
        $this->name_la = $data->name_la;
    }
    public function destroy($ids)
    {
        $ids = $this->ID;
        $data = TourismType::find($ids);
        $data->delete();
        $this->dispatchBrowserEvent('hide-modal-delete');
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ສຳເລັດເເລ້ວ!',
            'icon' => 'success',
        ]);
        $this->resetform();
    }
}
