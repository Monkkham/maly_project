<?php

namespace App\Http\Livewire\Backend;

use App\Models\District;
use App\Models\ImageMulti;
use App\Models\Province;
use App\Models\PublicPostTourism;
use App\Models\TourismType;
use App\Models\Village;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class UpdatePublicPostTourismContent extends Component
{
    use WithFileUploads;
    public $name, $slug_id, $data, $tourism_type_id, $users_id, $phone, $start_lat_long, $email, $note, $role, $lats, $longs, $latitude, $longitude, $address;
    public $new_img, $publicPostTourismId, $img, $districts = [], $villages = [], $images = [], $province_id, $district_id, $image, $village_id, $tourism_images = [];
    public function mount($slug_id)
    {
        $public_post_tourism = PublicPostTourism::with('tourism_images')->find($slug_id);
        $this->name = $public_post_tourism->name;
        $this->tourism_type_id = $public_post_tourism->tourism_type_id;
        $this->users_id = $public_post_tourism->users_id;
        $this->province_id = $public_post_tourism->province_id;
        $this->district_id = $public_post_tourism->district_id;
        $this->village_id = $public_post_tourism->village_id;
        if ($this->image) {
            $this->image = $public_post_tourism->image;
        }
        $this->note = $public_post_tourism->note;
        $this->latitude = $public_post_tourism->latitude;
        $this->longitude = $public_post_tourism->longitude;
        // Extract images from the loaded relationship
        // $this->images = $public_post_tourism->tourism_images->pluck('photo')->toArray();
        $this->publicPostTourismId = $slug_id;
        $this->images = PublicPostTourism::with('tourism_images')->find($this->publicPostTourismId);
    }
    public function render()
    {
        $tourism_type = TourismType::all();
        $provinces = Province::all();
        if ($this->province_id) {
            $this->districts = District::where('province_id', $this->province_id)->get();
        }
        if ($this->district_id) {
            $this->villages = Village::where('district_id', $this->district_id)->get();
        }
        return view('livewire.backend.update-public-post-tourism-content', compact('provinces', 'tourism_type'))->layout('layouts.backend.style');
    }
    protected $rules = [
        'name' => 'required|string',
        'tourism_type_id' => 'required|integer',
        'users_id' => 'required|integer',
        'village_id' => 'required|integer',
        'district_id' => 'required|integer',
        'note' => 'nullable|string',
        'latitude' => 'required|numeric',
        'longitude' => 'required|numeric',
        'image' => 'nullable|image|mimes:jpg,png,jpeg',
        'images.*' => 'nullable|image|mimes:jpg,png,jpeg', // Validation rule for each image in the array
    ];
    public function Update()
    {
        if ($this->start_lat_long) {
            $start_lat_long = $this->start_lat_long;
            $start_lat_long = str_replace(',', '.', $start_lat_long);
            list($end_lat, $end_long) = explode('. ', $start_lat_long);
        }
        $this->data = PublicPostTourism::with('tourism_images')->find($this->publicPostTourismId);
        if (!$this->data) {
            throw new \Exception('PublicPostTourism not found');
        }
        $this->data->name = $this->name;
        $this->data->tourism_type_id = $this->tourism_type_id;
        $this->data->users_id = $this->users_id;
        $this->data->village_id = $this->village_id;
        $this->data->district_id = $this->district_id;
        $this->data->note = $this->note;
        if ($this->start_lat_long) {
            $this->data->latitude = $end_lat;
            $this->data->longitude = $end_long;
        }
        if ($this->image) {
            $this->validate([
                'image' => 'required|mimes:jpg,png,jpeg',
            ]);
            $filename_image = $this->image->getClientOriginalName();
            $this->image->storeAs('upload/tourism_post' . $filename_image);
            $this->data->image = 'upload/tourism_post' . $filename_image;
        }
        $this->data->save();
        if ($this->images != '') {
            foreach ($this->images as $key => $photo) {
                $multi = new ImageMulti();
                $multi->public_post_tourism_id = $this->data->id;
                $imageName = Carbon::now()->timestamp . $key . '.' . $this->images[$key]->extension();
                $this->images[$key]->storeAs('upload/multi_post', $imageName);
                $multi->photo = 'upload/multi_post/' . $imageName;
                $multi->save();
            }
        }
        $this->images = '';
        session()->flash('success', 'ແກ້ໄຂສຳເລັດເເລ້ວ!');
        // return redirect(route('backend.Tourism'));
        return redirect(route('backend.UpdatePublicPostTourism', $this->slug_id));

    }
    public function deleteImage($imageId)
    {
        $image = ImageMulti::find($imageId);
        if ($image) {
            // Delete the image file from storage
            if (Storage::disk('public')->exists($image->photo)) {
                Storage::disk('public')->delete($image->photo);
            }
            $image->delete();
            $this->dispatchBrowserEvent('swal', [
                'title' => 'ສຳເລັດເເລ້ວ!',
                'icon' => 'success',
            ]);
            return redirect(route('backend.UpdatePublicPostTourism', $this->slug_id));
        } else {
            session()->flash('message', 'Image not found.');
        }
    }
}
