<?php

namespace App\Http\Livewire\Backend;

use App\Models\District;
use App\Models\ImageMulti;
use App\Models\Province;
use App\Models\PublicPostTourism;
use App\Models\TourismType;
use App\Models\Village;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithFileUploads;

class CreatePublicPostTourismContent extends Component
{
    use WithFileUploads;
    public $name, $tourism_type_id, $phone, $email, $note, $role, $lats, $longs, $latitude, $longitude, $address,$start_lat_long;
    public $new_img, $img, $districts = [], $villages = [], $images = [], $province_id = 7, $district_id, $image, $village_id;
    public function render()
    {
        $tourism_type = TourismType::all();
        $provinces = Province::all();
        if ($this->province_id) {
            $this->districts = District::where('province_id', $this->province_id)->get();
        }
        if ($this->district_id) {
            $this->villages = Village::where('district_id', $this->district_id)->get();
        }
        return view('livewire.backend.create-public-post-tourism-content', compact('provinces', 'tourism_type'))->layout('layouts.backend.style');
    }
    public function resetField()
    {
        $this->images = '';
    }
    public function CreatePostTourism()
    {
        $this->validate([
            'start_lat_long' => 'required',
            'tourism_type_id' => 'required',
            'village_id' => 'required',
            'district_id' => 'required',
            'province_id' => 'required',
            'name' => 'required',
            'note' => 'required',
        ], [
            'start_lat_long.required' => 'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'tourism_type_id.required' => 'ເລືອກຂໍ້ມູນກ່ອນ!',
            'village_id.required' => 'ເລືອກຂໍ້ມູນກ່ອນ!',
            'district_id.required' => 'ເລືອກຂໍ້ມູນກ່ອນ!',
            'province_id.required' => 'ເລຶອກຂໍ້ມູນກ່ອນ!',
            'name.required' => 'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'note.required' => 'ປ້ອນຂໍ້ມູນກ່ອນ!',
        ]);
        $start_lat_long = $this->start_lat_long;
        $start_lat_long = str_replace(',', '.', $start_lat_long);
        list($latitude, $longitude) = explode('. ', $start_lat_long);
        try {
            DB::beginTransaction();
            $data = new PublicPostTourism();
            $data->tourism_type_id = $this->tourism_type_id;
            $data->users_id = auth()->user()->id;
            $data->village_id = $this->village_id;
            $data->district_id = $this->district_id;
            $data->province_id = $this->province_id;
            $data->name = $this->name;
            $data->note = $this->note;
            $data->latitude = $latitude;
            $data->longitude = $longitude;
            if (!empty($this->image)) {
                $this->validate([
                    'image' => 'required|mimes:jpg,png,jpeg',
                ]);
                $imageName = Carbon::now()->timestamp . '.' . $this->image->extension();
                $this->image->storeAs('upload/tourism_post', $imageName);
                $data->image = 'upload/tourism_post' . '/' . $imageName;
            } else {
                $data->image = '';
            }
            $data->save();
            if ($this->images != '') {
                foreach ($this->images as $key => $photo) {
                    $multi = new ImageMulti();
                    $multi->public_post_tourism_id = $data->id;
                    $imageName = Carbon::now()->timestamp . $key . '.' . $this->images[$key]->extension();
                    $this->images[$key]->storeAs('upload/multi_post', $imageName);
                    $multi->photo = 'upload/multi_post/' . $imageName;
                    $multi->save();
                }
            }
            $this->images = '';
            DB::commit();
            // $this->dispatchBrowserEvent('swal', [
            //     'title' => 'ເຜີຍແຜ່ຂໍ້ມູນສຳເລັດເເລ້ວ!',
            //     'icon' => 'success',
            // ]);
            session()->flash('success', 'ເຜີຍແຜ່ຂໍ້ມູນສຳເລັດເເລ້ວ!');
            return redirect()->route('backend.Tourism');
        } catch (\Exception $ex) {
            DB::rollBack();
            $this->dispatchBrowserEvent('swal', [
                'title' => 'ມີບາງຢ່າງຜິດພາດ!',
                'icon' => 'warning',
            ]);
        }
    }
}
