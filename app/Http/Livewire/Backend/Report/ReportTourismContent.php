<?php

namespace App\Http\Livewire\Backend\Report;

use App\Models\PublicPostTourism;
use Livewire\Component;

class ReportTourismContent extends Component
{
    public $start_date, $end_date;
    public function mount()
    {
        $this->start_date = date('Y-m-d');
        $this->end_date = date('Y-m-d');
    }
    public function render()
    {
        $end = date('Y-m-d H:i:s', strtotime($this->end_date . '23:23:59'));
        if ($this->start_date && $this->end_date) {
            $data = PublicPostTourism::whereBetween('created_at', [$this->start_date, $end])->get();
        } else {
            $data = [];
        }
        return view('livewire.backend.report.report-tourism-content', compact('data'))->layout('layouts.backend.style');
    }
}
