<?php

namespace App\Http\Livewire\Backend\Report;

use App\Models\Role;
use App\Models\User;
use Livewire\Component;

class ReportUsersContent extends Component
{
    public $start_date, $end_date,$roles_id;
    public function mount()
    {
        $this->start_date = date('Y-m-d');
        $this->end_date = date('Y-m-d');
    }
    public function render()
    {
        $end = date('Y-m-d H:i:s', strtotime($this->end_date . '23:23:59'));
        $roles = Role::all();
        $data = User::get();
        if ($this->start_date && $this->end_date) {
            $data = $data->whereBetween('created_at', [$this->start_date, $end]);
        } else {
            $data = [];
        }
        if($this->roles_id)
        {
            $data = $data->where('roles_id',$this->roles_id);
        }
        return view('livewire.backend.report.report-users-content',compact('data','roles'))->layout('layouts.backend.style');
    }
}
