<?php

namespace App\Http\Livewire;

use App\Models\Visitor;
use Illuminate\Support\Facades\Request;
use Livewire\Component;

class VisitorCounter extends Component
{
    public $visitorCount;

    public function mount()
    {
        $this->storeVisitor();
        $this->visitorCount = Visitor::count();
    }

    public function storeVisitor()
    {
       // Save visitor information to database
       $visitor = new Visitor();
       $visitor->ip_address = Request::ip();
       // Add other fields as needed, e.g., user agent, timestamp, etc.
       $visitor->save();
    }

    public function render()
    {
        return view('livewire.visitor-counter');
    }
}
