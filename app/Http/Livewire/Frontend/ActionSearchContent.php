<?php

namespace App\Http\Livewire\Frontend;

use App\Models\PublicPostTourism;
use Livewire\Component;

class ActionSearchContent extends Component
{
    public $search;
    public function render()
    {
        $data = PublicPostTourism::orderBy('id', 'desc')->where(function ($q) {
            $q->where('name', 'like', '%' . $this->search . '%')
                ->orwhere('note', 'like', '%' . $this->search . '%');
        })->get();
        return view('livewire.frontend.action-search-content', compact('data'));
    }
}
