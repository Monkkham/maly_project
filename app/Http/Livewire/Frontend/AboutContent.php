<?php

namespace App\Http\Livewire\Frontend;

use App\Models\Office;
use App\Models\User;
use Livewire\Component;

class AboutContent extends Component
{
    public $about;
    public function render()
    {
        $this->about = Office::first();
        $employee = User::where('roles_id',2)->get();
        return view('livewire.frontend.about-content',compact('employee'))->layout('layouts.frontend.style');
    }
}
