<?php

namespace App\Http\Livewire\Frontend;

use App\Models\User;
use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class SignupContent extends Component
{
    public $name_lastname, $phone, $password,$confirmPassword;
    public function render()
    {
        return view('livewire.frontend.signup-content')->layout('layouts.frontend.style');
    }
    public function SignUp()
    {
        $this->validate([
            'name_lastname' => 'required',
            'phone' => 'required|min:8|unique:users',
            'password' => 'required|min:6',
            'confirmPassword' => 'required|same:password',
        ], [
            'name_lastname.required' => 'ປ້ອນຊື່ ນາມສະກຸນກ່ອນ!',
            'phone.required' => 'ປ້ອນເບີໂທກ່ອນ!',
            'phone.unique' => 'ເບີໂທນີ້ມີໃນລະບົບເເລ້ວ!',
            'phone.min' => 'ເບີໂທ8ໂຕເລກຂື້ນໄປ!',
            'password.required' => 'ປ້ອນລະຫັດຜ່ານກ່ອນ!',
            'password.min' => 'ລະຫັດ6ຕົວຂື້ນໄປ!',
            'confirmPassword.required' => 'ປ້ອນຍືນຍັນລະຫັດກ່ອນ!',
            'confirmPassword.same' => 'ລະຫັດຜ່ານ ເເລະ ຍືນຍັນລະຫັດບໍ່ຕົງກັນ!',
        ]);
        $data = new User();
        $data->name_lastname = $this->name_lastname;
        $data->phone = $this->phone;
        $data->password = Hash::make($this->password);
        $data->gender = 1;
        $data->email = 'example@gmail.com';
        $data->status = 1;
        $data->roles_id = 3;
        $data->save();
        Auth::guard('admin')->login($data);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ລົງທະບຽນສຳເລັດເເລ້ວ!',
            'icon' => 'success',
        ]);
        return redirect(route('frontend.home'));
    }
}
