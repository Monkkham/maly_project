<?php

namespace App\Http\Livewire\Frontend;

use App\Models\PublicPostTourism;
use Illuminate\Support\Facades\Redirect;
use Livewire\Component;

class TourismsContent extends Component
{
    public $slug_id;
    public function render()
    {
        $all_tourism = PublicPostTourism::orderBy('id', 'desc')->get(); //ທຳມະຊາດ
        return view('livewire.frontend.tourisms-content', compact('all_tourism'))->layout('layouts.frontend.style');
    }
    public function viewer($ids)
    {
        $data = PublicPostTourism::find($ids);
        $data->viewer += 1;
        $data->save();
    }
    public function DetailTourism($slug_id)
    {
        $this->viewer($slug_id);
        return redirect(route('frontend.TourismDetailContent', $slug_id));
    }
}
