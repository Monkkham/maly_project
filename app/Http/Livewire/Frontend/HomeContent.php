<?php

namespace App\Http\Livewire\Frontend;

use App\Models\PublicPostTourism;
use App\Models\Slide;
use App\Models\TourismType;
use Livewire\Component;

class HomeContent extends Component
{
    public $tourism_type_natures, $tourism_type_historys, $tourism_type_cultures, $slug_id, $search;
    public function render()
    {
        $data = Slide::all();
        $this->tourism_type_natures = TourismType::orderBy('id', 'desc')->where('id', 1)->first(); //ທຳມະຊາດ
        $this->tourism_type_historys = TourismType::orderBy('id', 'desc')->where('id', 2)->first(); //ປະຫວັດສາດ
        $this->tourism_type_cultures = TourismType::orderBy('id', 'desc')->where('id', 3)->first(); //ວັດທະນາທຳ

        $tourism_type_nature = PublicPostTourism::orderBy('id', 'desc')->where('tourism_type_id', 1)->get(); //ທຳມະຊາດ
        $tourism_type_history = PublicPostTourism::orderBy('id', 'desc')->where('tourism_type_id', 2)->get(); //ປະຫວັດສາດ
        $tourism_type_culture = PublicPostTourism::orderBy('id', 'desc')->where('tourism_type_id', 3)->get(); //ວັດທະນາທຳ
        return view('livewire.frontend.home-content', compact('data', 'tourism_type_nature', 'tourism_type_history', 'tourism_type_culture'))->layout('layouts.frontend.style');
    }
    public function viewer($ids)
    {
        $data = PublicPostTourism::find($ids);
        $data->viewer += 1;
        $data->save();
    }
    public function DetailTourism($slug_id)
    {
        $this->viewer($slug_id);
        return redirect(route('frontend.TourismDetailContent', $slug_id));
    }
}
