<?php

namespace App\Http\Livewire\Frontend;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;

class SigninContent extends Component
{
    public $phone, $password, $remember;
    public function render()
    {
        return view('livewire.frontend.signin-content')->layout('layouts.frontend.style');
    }
    public function Signin()
    {
        $this->validate([
            'phone' => 'required',
            'password' => 'required',
        ], [
            'phone.required' => 'ກະລຸນາປ້ອນເບີໂທກ່ອນ!',
            'password.required' => 'ກະລຸນາປ້ອນລະຫັດຜ່ານກ່ອນ!',
        ]);
        if (Auth::guard('admin')->attempt([
            'phone' => $this->phone,
            'password' => $this->password],
            $this->remember)) {
                $this->dispatchBrowserEvent('swal', [
                    'title' => 'ເຂົ້າສູ່ລະບົບສຳເລັດ!',
                    'icon' => 'success',
                ]);
            return redirect(route('frontend.home'));
        } else {
            $this->dispatchBrowserEvent('swal', [
                'title' => 'ເບີໂທ ຫລື ລະຫັດບໍ່ຖືກຕ້ອງລອງໃຫມ່!',
                'icon' => 'warning',
            ]);
            // return redirect(route('frontend.signin'));
        }
    }
}
