<?php

namespace App\Http\Livewire\Frontend;

use Livewire\Component;
use App\Models\PublicPostTourism;

class SearchContent extends Component
{
    public $search, $datas;
    public function mount()
    {
        $this->datas = 'Allprojects';
        $this->fill(request()->only('search'));
    }
    public $slug_id;
    public function render()
    {
        $data = PublicPostTourism::where(function ($q) {
            $q->where('name', 'like', '%' . $this->search . '%')
                ->orwhere('note', 'like', '%' . $this->search . '%');
        })->get();
        return view('livewire.frontend.search-content',compact('data'))->layout('layouts.frontend.style');
    }
    public function DetailTourism($slug_id)
    {
        return redirect(route('frontend.TourismDetailContent', $slug_id));
    }
}
