<?php

namespace App\Http\Livewire\Frontend;

use App\Models\PublicPostTourism;
use Livewire\Component;

class TourismDetailContent extends Component
{
    public $name, $slug_id, $tourism_type, $users_id,$created_at, $viewer,$phone, $email, $note, $role, $lats, $longs, $latitude, $longitude, $address;
    public $new_img, $publicPostTourismId, $img, $districts = [], $villages = [], $images = [], $province, $district, $image, $village;
    public function mount($slug_id)
    {
        $public_post_tourism = PublicPostTourism::with('tourism_images')->find($slug_id);
        $this->name = $public_post_tourism->name;
        $this->tourism_type = $public_post_tourism->tourism_type->name_la;
        $this->users_id = $public_post_tourism->users_id;
        $this->province = $public_post_tourism->province->name_la;
        $this->district = $public_post_tourism->district->name_la;
        $this->village = $public_post_tourism->village->name_la;
        $this->image = $public_post_tourism->image;
        $this->note = $public_post_tourism->note;
        $this->latitude = $public_post_tourism->latitude;
        $this->longitude = $public_post_tourism->longitude;
        $this->created_at = $public_post_tourism->created_at;
        $this->viewer = $public_post_tourism->viewer;
        // Extract images from the loaded relationship
        // $this->images = $public_post_tourism->tourism_images->pluck('photo')->toArray();
        $this->publicPostTourismId = $slug_id;
        $this->images = PublicPostTourism::with('tourism_images')->find($this->publicPostTourismId);
    }
    public function render()
    {
        return view('livewire.frontend.tourism-detail-content')->layout('layouts.frontend.style');
    }
}
