<?php

namespace App\Http\Livewire\Frontend;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;

class SignoutContent extends Component
{
    public function render()
    {
        return view('livewire.frontend.signout-content');
    }
    public function SignOut()
    {
        Auth::logout();
        session()->flash('success', 'ອອກຈາກລະບົບສຳເລັດ!');
        return redirect(route('frontend.home'));
    }
}
